# Rotating Shapes in 8086 Assembly

This is a simple Assembly file that draws graphics with rotating shapes on the screen.
It runs on the Intel 8086 16-bit microprocessor architecture, but the easiest way to run it on modern machines is to run it in Dosbox.

![A green rotating hexagon](pixel3.png)

![Two purple intertwined rotating triangles](pixel5.png)

![A green rotating depiction of an octahedron](pixel7.png)

![Three rotating triangles inside each other, colored red, purple and orange from the inside out](pixel8.png)

![Ditto, with different colors, orange, white and gray from the inside out](pixel10.png)

## Running

The easiest way to run the executable is to open dosbox in this directory:

`dosbox .`

And then run PIXEL.EXE

`C:\> PIXEL`

If you want to compile from source, please see below.

## Compiling

For the executables to work you need to install Dosbox.

In order to compile .asm files you need TASM (Turbo Assembler)
Just do

`tasm.exe source.asm`
`tlink.exe source.obj`
`source.exe`

There shouldn't be any errors in the .asm files.
Contact me for more information.
